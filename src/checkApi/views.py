from django.views.generic import TemplateView
from django.http import HttpResponse, JsonResponse, FileResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from mydjango.settings import HOST
from .tasks import htmlToPdf
from .models import Printer, Order, Check

from urllib.parse import urlparse
import json
import base64

@csrf_exempt
def createCheck(request):
    if request.method == 'POST':
        order = json.loads(request.body)['order']
        print(order)
        
        # check existense of checks
        printers = Printer.objects.filter(point_id=order['point_id'])
        if not printers.exists():
            return JsonResponse({"error" : "Для данной точки не настроено ни одного принтера" }, status=404)
        elif Order.objects.filter(order_id=order['id']).exists():
            return JsonResponse({"error" : "Для данного заказа уже созданы чеки" }, status=404)
        else:
            order = Order.objects.create(
                order_id=order['id'],
                items=order['items'],
                price=order['price'],
                address=order['address'],
                client=order['client'],
                point_id=order['point_id']
                )
            for printer in printers:
                check = Check.objects.create(
                    printer=printer,
                    order=order,
                    type=printer.check_type
                    )

                htmlToPdf.apply(args=[check.id])

        return JsonResponse({"ok" : "Чеки успешно созданы" })


def newChecks(request):
    try:
        api_key = request.GET['api_key']
    except KeyError:
        return JsonResponse({"error" : "Bad request" }, status=400)

    # auth check 
    try:
        printer = Printer.objects.get(api_key=api_key)
    except ObjectDoesNotExist:
        return JsonResponse({"error" : "Ошибка авторизации" }, status=401)
    checks = Check.objects.filter(printer=printer, status=1)
    url = HOST + "check/"
    return JsonResponse({"checks": [{
        "id": check.order.order_id,
        "url": url + \
        "?order_id={order_id}&type={type}&format={format}"
        .format(
            order_id=check.order.order_id,
            type = 'client' if check.type==1 else 'kitchen',
            format='pdf'
            )
    } for check in checks]}, status=200)


def checkPoint(request):
    try:
        order_id = request.GET['order_id']

        type = request.GET['type']
        if type == 'client' or type == 'kitchen':
            type = 1 if type=='client' else 0
        else:
            raise KeyError
        format = request.GET['format']
        api_key = request.GET['api_key']
    except KeyError:
        return JsonResponse({"error" : "Bad request" }, status=400)

    
    # auth check 
    try:
        printer = Printer.objects.get(api_key=api_key)
    except ObjectDoesNotExist:
        return JsonResponse({"error" : "Ошибка авторизации" }, status=401)

    check = Check.objects.filter(printer=printer, type=type, order__order_id=order_id).first()
    

    if not check:
        return JsonResponse({"error" : "Для данного заказа нет чеков" }, status=400)
    if format == 'html':
        template = 'client_check.html' if check.type else 'kitchen_check.html'
        return FileResponse(render_to_string(template, {'order': check.order}))
    elif check.status == 1:
        check.status = 2
        check.save()
        return FileResponse(open(check.pdf_file.path, 'rb'), content_type='application/pdf')
    else:
        return JsonResponse({"ok" : "Для данного заказа не сгенерирован чек в формате PDF" })    

    
def home(request):
    return render(request, "home.html")