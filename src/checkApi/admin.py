from django.contrib import admin

from .models import *


class OrderAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'price', 'point_id')


class PrinterAdmin(admin.ModelAdmin):
    list_filter = ('check_type',)
    list_display = ('name', 'api_key', 'check_type', 'point_id')


class CheckAdmin(admin.ModelAdmin):
    list_filter = ('type', 'status',  'printer__name', 'order')
    list_display = ('order', 'printer', 'type', 'status')


# Register your models here.
admin.site.register(Order, OrderAdmin)
admin.site.register(Printer, PrinterAdmin)
admin.site.register(Check, CheckAdmin)
