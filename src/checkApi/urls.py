from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^create_checks/$', views.createCheck, name='createCheck'),
    url(r'^new_check/$', views.newChecks, name='newChecks'),
    url(r'^check/$', views.checkPoint, name='checkPoint'),
    url(r'^$', views.home, name='home'),
]
