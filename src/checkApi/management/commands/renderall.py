from checkApi.models import Check
from checkApi.tasks import htmlToPdf
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        checks = Check.objects.all()
        for check in checks:
            if check.status == 0:
                htmlToPdf.apply(args=[check.id])
