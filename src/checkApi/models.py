from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import gettext as _

CHECK_TYPE_CHOICE = (
    (0, _("kitchen")),
    (1, _("client"))
)
CHECK_STATUS_CHOICE = (
    (0, _("new")),
    (1, _("rendered")),
    (2, _("printed"))
)


class Printer(models.Model):
    name = models.CharField(max_length=50)
    api_key = models.CharField(max_length=50, unique=True)
    check_type = models.IntegerField(choices=CHECK_TYPE_CHOICE, default=0)
    point_id = models.IntegerField()

    def __str__(self):
        return self.name


class Order(models.Model):
    order_id = models.IntegerField(unique=True)
    items = JSONField()
    price = models.IntegerField()
    address = models.CharField(max_length=255)
    client = JSONField()
    point_id = models.IntegerField()

    def __str__(self):
        return "order " + str(self.order_id)


class Check(models.Model):
    printer = models.ForeignKey(Printer, on_delete=models.CASCADE)
    type = models.IntegerField(choices=CHECK_TYPE_CHOICE, default=0)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    status = models.IntegerField(choices=CHECK_STATUS_CHOICE, default=0)
    pdf_file = models.FileField(blank=True)
