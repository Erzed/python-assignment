import base64
import json
import logging
import os.path

import requests
from django.conf import settings
from django.template.loader import render_to_string
from mydjango.celery import app

from .models import Check


logger = logging.getLogger("celery")


@app.task
def htmlToPdf(check_id):
    logger.info('Creating check. Check id = ' + str(check_id))

    url = 'http://pdfer:80/'
    encoding = 'utf-8'

    check = Check.objects.get(pk=check_id)
    template = 'client_check.html' if check.type else 'kitchen_check.html'
    base64_bytes = base64.b64encode(render_to_string(template, {'order': check.order}).encode())
    base64_string = base64_bytes.decode(encoding)

    data = {
        'contents': base64_string,
    }
    headers = {
        'Content-Type': 'application/json',
    }

    try:
        response = requests.post(url, data=json.dumps(data), headers=headers)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        logger.error(err)
        return
    except requests.exceptions.ConnectionError as err:
        logger.error(err)
        return
    finally:
        # todo re-init
        pass
    output_file_name = str(check.order.order_id) + "_" + ("client" if check.type else "kitchen") + '.pdf'
    output_file_path = os.path.join(settings.MEDIA_ROOT, output_file_name)

    with open(output_file_path, 'wb') as f:
        f.write(response.content)

    check.status = 1
    check.pdf_file = output_file_name
    check.save()
