FROM python:latest
ENV PYTHONUNBUFFERED 1

RUN mkdir /static
RUN mkdir /src
WORKDIR /src
ADD requirements.txt /src/
RUN pip3 install -r requirements.txt
ADD . /src/
CMD python manage.py migrate; gunicorn mydjango.wsgi -b 0.0.0.0:8000 & celery worker --app=checkApi.tasks