build:
	docker-compose build
	docker-compose run web /bin/bash -c "python manage.py collectstatic --no-input;python manage.py makemigrations; python manage.py migrate; python manage.py loaddata db.json"

up:
	docker-compose up

prerender:
	docker-compose run web /bin/bash -c "python manage.py renderall"

createuser:
	docker-compose run web python manage.py createsuperuser

	
